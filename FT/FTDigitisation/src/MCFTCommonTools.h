#ifndef MCFTCOMMONTOOLS_H
#define MCFTCOMMONTOOLS_H 1

// from CLHEP
#include "CLHEP/Random/RandomEngine.h"

// from Gaudi
#include "GaudiKernel/IRndmEngine.h"

namespace MCFTCommonTools
{

class HepRndmEngnIncptn : public CLHEP::HepRandomEngine {
public:
  double flat() override {
    return m_gaudiRndmEngine->rndm();
  }

  void flatArray(const int size, double* vect) override {
    std::vector<double> rndmNumbers(size, 0.);
    m_gaudiRndmEngine->rndmArray(rndmNumbers, size);
    std::copy(rndmNumbers.begin(), rndmNumbers.end(), vect);
  }
  

  void setSeed(long seed, int) override {
    m_gaudiRndmEngine->setSeeds({seed});
  };
  void setSeeds(const long * seeds, int size) override {
    const std::vector<long> seed_vec(seeds, seeds+size);
    m_gaudiRndmEngine->setSeeds(seed_vec);
  }

  void saveStatus( const char filename[] = "Config.conf") const override {
    //std::cout << "Will not saveStatus in file " << filename << std::endl;
    throw GaudiException( std::string("Will not saveStatus in file ") + filename,
                          "MCFTCommonTools::saveStatus", StatusCode::FAILURE );

  };
  void restoreStatus( const char filename[] = "Config.conf" ) override {
    throw GaudiException( std::string("Will not restoreStatus in file ") + filename,
                          "MCFTCommonTools::restoreStatus", StatusCode::FAILURE );
  };
  void showStatus() const override {};
  std::string name() const override {return "HepRndmEngnIncptn";}

  void setGaudiRndmEngine(IRndmEngine* gaudiRndmEngine){
    m_gaudiRndmEngine = gaudiRndmEngine;
  }

private:
  IRndmEngine* m_gaudiRndmEngine = nullptr;

}; 



} // namespace MCFTCommonTools

#endif //MCFTCOMMONTOOLS_H
