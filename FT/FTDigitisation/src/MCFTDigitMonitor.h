#ifndef MCFTDIGITMONITOR_H 
#define MCFTDIGITMONITOR_H 1

// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiAlg/Consumer.h"

// from FTDet
#include "FTDet/DeFTDetector.h"

// from FTEvent
#include "Event/MCFTDigit.h"

/** @class MCFTDigitMonitor MCFTDigitMonitor.h
 *  
 *
 *  @author Eric Cogneras, Luca Pescatore
 *  @date   2012-07-05
 */

class MCFTDigitMonitor : public Gaudi::Functional::Consumer
                         <void(const LHCb::MCFTDigits&),
                         Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>> {
public: 
  
  MCFTDigitMonitor(const std::string& name,ISvcLocator* pSvcLocator);

  StatusCode initialize() override;
  void operator()(const LHCb::MCFTDigits& digits) const override ;

private:

  void fillHistograms(const LHCb::MCFTDigit* mcDigit,
                      const std::set<const LHCb::MCHit*>& mcHits,
                      const std::string& hitType) const;

  DeFTDetector* m_deFT = nullptr; ///< pointer to FT detector description

};
#endif // MCFTDIGITMONITOR_H
