// from FTEvent
#include "Event/MCFTDeposit.h"

// local
#include "FTClusterCreator.h"

//-----------------------------------------------------------------------------
// Implementation file for class : FTClusterCreator
//
// 2012-04-06 : Eric Cogneras
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FTClusterCreator )

FTClusterCreator::FTClusterCreator(const std::string& name, ISvcLocator* pSvcLocator) :
       MultiTransformer(name, pSvcLocator,
       { KeyValue{"InputLocation", LHCb::MCFTDigitLocation::Default} },
       { KeyValue{"OutputLocation", LHCb::FTLiteClusterLocation::Default},
         KeyValue{"FullClusterPath", LHCb::FTClusterLocation::Default},
         KeyValue{"MCToClusterLocation",
           Links::location(LHCb::FTLiteClusterLocation::Default)},
         KeyValue{"MCToClusterExtLocation",
           Links::location(LHCb::FTLiteClusterLocation::Default+"WithSpillover")},
         KeyValue{"HitToClusterLocation",
           Links::location(LHCb::FTLiteClusterLocation::Default + "2MCHits")},
         KeyValue{"HitToClusterExtLocation",
           Links::location(LHCb::FTLiteClusterLocation::Default + "2MCHitsWithSpillover")}
         } ) {}


//=============================================================================
// Main execution
//=============================================================================
std::tuple<FTLiteClusters, LHCb::FTClusters,
           LHCb::LinksByKey,LHCb::LinksByKey,
           LHCb::LinksByKey,LHCb::LinksByKey>
FTClusterCreator::operator()(const LHCb::MCFTDigits& digits) const {

  // Create the objects to be returned and reserve memory.
  OutputLinks<ContainedObject, LHCb::MCParticle> mcToClusterLink{};
  OutputLinks<ContainedObject, LHCb::MCParticle> mcToClusterLinkExtended{};
  OutputLinks<ContainedObject, LHCb::MCHit> hitToClusterLink{};
  OutputLinks<ContainedObject, LHCb::MCHit> hitToClusterLinkExtended{};

  using FTLiteClusters = FastClusterContainer<LHCb::FTLiteCluster,int>;

  FTLiteClusters liteClusterCont{};
  liteClusterCont.reserve(15e3);
  LHCb::FTClusters clusterCont{};
  if( m_writeFullClusters ) clusterCont.reserve(12e3);

  //***********************
  //* MAIN LOOP
  //***********************

  // Digit Container is sorted wrt channelID
  auto digitIter = digits.begin();
  while(digitIter != digits.end()){
    // loop over digits
    LHCb::MCFTDigit* digit = *digitIter;
    // Check if digit is above threshold1
    if( (!m_usePEnotADC && digit->adcCount() >= m_adcThreshold1) ||
        (m_usePEnotADC && digit->photoElectrons() >= m_adcThreshold1) ) {

      // ADC above seed : start clustering
      if ( msgLevel( MSG::VERBOSE) )
        verbose() << " ---> START NEW CLUSTER WITH SEED @ " << digit->channelID() << endmsg;


      // list of contributing MCParticles (MCHits)
      std::set< const LHCb::MCParticle*> contributingMCParticles;
      std::set< const LHCb::MCHit*> contributingMCHits;

      //loop till below threshold1 or end, optionally exit when cluster bigger than a limit
      auto startClusIter = digitIter;   // begin channel of cluster
      auto stopClusIter = digitIter;    // end channel of cluster
      while(    (++digitIter != digits.end())
             && (  (!m_usePEnotADC && (*digitIter)->adcCount() >= m_adcThreshold1)
                 || (m_usePEnotADC && (*digitIter)->photoElectrons() >= m_adcThreshold1)  )
             && (!m_fragBigCluster || ((stopClusIter - startClusIter + 1) < m_clusterMaxWidth))
           ) {

        // next digit in the same SiPM, and neighbour channel
        if(    ((*digitIter)->channelID().uniqueSiPM() == (*startClusIter)->channelID().uniqueSiPM())
            && ((*digitIter)->channelID().channel()==((*stopClusIter)->channelID().channel()+1))) {
           stopClusIter = digitIter;
        }
        else break;    //end
      }


      // calculate the cluster charge / mean position
      if ( msgLevel( MSG::VERBOSE) )
        verbose() << " ---> Done with cluster finding, now calculating charge / frac.pos" << endmsg;

      float    totalCharge  = 0.0;
      double   totalChargePE= 0.0;
      double   wsumPosition = 0.0;
      //std::vector<LHCb::MCFTDigit*> digitVec; //TODO
      for(auto clusDigitIter = startClusIter; clusDigitIter <(stopClusIter+1); ++clusDigitIter) {
        // Loop over digits in to-be cluster

        LHCb::MCFTDigit * clusDigit = * clusDigitIter;
        //digitVec.push_back(clusDigit); //TODO

        // calculate channel 'weights' as seen by the hardware thresholds
        // (2bits implementation: 3 adc thresholds represent some #PE)
        // (threshold1 < threshold2 < threshold3)
        float channelWeight = ( m_usePEnotADC     ?  clusDigit -> photoElectrons()        :
				clusDigit->adcCount() >= m_adcThreshold3  ?  m_adcThreshold3Weight.value()        :
				clusDigit->adcCount() >= m_adcThreshold2  ?  m_adcThreshold2Weight.value()        :
				clusDigit->adcCount() >= m_adcThreshold1  ?  m_adcThreshold1Weight.value()        : 0. );


        totalCharge += channelWeight;
        totalChargePE += clusDigit -> photoElectrons();

        // mean position will be [ (rel. pos. from left) * charge ] / totalCharge (see below)
        wsumPosition += (clusDigitIter-startClusIter) * channelWeight;

        // check contributions from MCHits, for linking
        for (const auto& deposit : clusDigit->deposits() ) {
          const auto& mcHit = deposit->mcHit();
          if( mcHit != nullptr ) {
            contributingMCHits.insert(mcHit);
            if( mcHit->mcParticle() != nullptr )
              contributingMCParticles.insert(mcHit->mcParticle());
          }
        }
      } // end of loop over digits in 'cluster'


      // test cluster size, then  analyse big clusters
      unsigned int widthClus = (stopClusIter-startClusIter+1);
      if(widthClus >= m_clusterMinWidth) {

        // manage the 3 categories of clusters according to the config flags
        // (single, double adjacent (<= m_largeClusterSize), big (> m_largeClusterSize))
        // for now just 2 types : single and "big" for the others
        bool isLargeNotInFrag(false);
        bool isLargeInFrag(false);
        bool isEdgeInFrag(false);
        if(!m_fragBigCluster) {
          isLargeNotInFrag = (widthClus > m_largeClusterSize);
        }
        else {
          // test if part of a fragmented cluster
          bool isInFragBefore(false), isInFragAfter(false);
          if(startClusIter > digits.begin()) {
            if(((*(startClusIter-1))->channelID().channel() == ((*startClusIter)->channelID().channel()-1)) &&
               ((*(startClusIter-1))->channelID().uniqueSiPM() == (*startClusIter)->channelID().uniqueSiPM()) &&
               ((*(startClusIter-1))->adcCount() >= m_adcThreshold1)) {
               isInFragBefore=true;
            }
          }
          if(stopClusIter < digits.end() - 1) {
            if(((*(stopClusIter+1))->channelID().channel()==((*stopClusIter)->channelID().channel()+1)) &&
               ((*(stopClusIter+1))->channelID().uniqueSiPM() == (*stopClusIter)->channelID().uniqueSiPM()) &&
               ((*(stopClusIter+1))->adcCount() >= m_adcThreshold1)) {
               isInFragAfter=true;
            }
          }
          isLargeInFrag = (isInFragBefore || isInFragAfter);
          isEdgeInFrag = (isInFragBefore != isInFragAfter);
        }


        // keep or not the big clusters (fragmented or not)
        // if not keep only the edge clusters for the fragmented ones
        // don't apply further criteria on the total charge when in fragmented cluster
        if( ( m_keepBigCluster
            || (!m_fragBigCluster && !isLargeNotInFrag)
            || (m_fragBigCluster && (isEdgeInFrag || !isLargeInFrag)) )
            && ( isLargeInFrag
                || ( (widthClus==1 && totalCharge >= m_adcThreshold3Weight - 0.5)
                    || (widthClus> 1 && totalCharge >= m_adcThreshold1Weight.value() + m_adcThreshold2Weight.value() - 0.5)) )) {
          //define multipurpose flag
          bool isLarge = isLargeNotInFrag || isLargeInFrag;

          // compute position : when cluster is an edge of a double or big cluster keep
          // the middle of the cluster only (no weighting), otherwise use weights
          // also add channelID (uint) offset
          double clusPosition;
          if(isLargeInFrag) {     //or maybe for all large clusters
            clusPosition =(*startClusIter)->channelID() + float(widthClus-1)/2;
          }
          else {
            clusPosition =(*startClusIter)->channelID() + wsumPosition/totalCharge;
          }

          // The fractional position is defined in (-0.250, 0.750)
          unsigned int clusChanPosition = std::floor(clusPosition-m_lowestFraction);
          double fractionChanPosition = (clusPosition-clusChanPosition);
          int frac = int(2*(fractionChanPosition-m_lowestFraction));

          // Define new lite cluster and add to container
          liteClusterCont.emplace_back(clusChanPosition, frac, isLarge);

          // Optionally define new full cluster and add to container
          if( m_writeFullClusters ) {
            // Define new cluster and add to container
            LHCb::FTCluster *newCluster =
                new LHCb::FTCluster(clusChanPosition, fractionChanPosition, widthClus,
                                    m_storePECharge ? totalChargePE : totalCharge);
            clusterCont.insert(newCluster);
          }

          // Setup MCParticle to cluster link
          // (also for spillover / not accepted clusters, as long as it's not noise)
          for(const auto& i : contributingMCParticles) {
            mcToClusterLinkExtended.link(clusChanPosition, i ) ; // Needed for xdigi / xdst
            if ( i->parent()->registry()->identifier() ==
                 "/Event/"+LHCb::MCParticleLocation::Default)
              mcToClusterLink.link(clusChanPosition, i ) ;
          }
          // Setup MCHit to cluster link
          for(const auto& i : contributingMCHits ) {
            hitToClusterLinkExtended.link(clusChanPosition, i ) ; // Needed for xdigi / xdst
            if ( i->parent()->registry()->identifier() == "/Event/"+LHCb::MCHitLocation::FT)
              hitToClusterLink.link(clusChanPosition, i ) ;
          }
        }

      } // end of Cluster satisfies min size requirements
    }  // end digit above threshold1
    else digitIter++;   //increment when not above threshold1

  } // END of loop over Digits

  if (UNLIKELY(msgLevel(MSG::DEBUG))) {
     debug() << "Number of FTLiteClusters created: " << liteClusterCont.size() << endmsg;
  }

  // Extra checks
  static_assert(std::is_move_constructible<FTLiteClusters>::value,
                "FTLiteClusters must be move constructible for this to work.");
  static_assert(std::is_move_constructible<LHCb::FTClusters>::value,
                "FTClusters must be move constructible for this to work.");

  return std::make_tuple(std::move(liteClusterCont), std::move(clusterCont),
                         std::move(mcToClusterLink.links()),
                         std::move(mcToClusterLinkExtended.links()),
                         std::move(hitToClusterLink.links()),
                         std::move(hitToClusterLinkExtended.links()));
}
