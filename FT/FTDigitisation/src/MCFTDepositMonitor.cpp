// local
#include "MCFTDepositMonitor.h"

#include <range/v3/all.hpp>

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( MCFTDepositMonitor )

MCFTDepositMonitor::MCFTDepositMonitor(const std::string& name,ISvcLocator* pSvcLocator) :
Consumer(name, pSvcLocator,
    KeyValue{"DepositLocation", LHCb::MCFTDepositLocation::Default}) {}

namespace {
  static const auto FTMCHitLocation = "/Event/"+ LHCb::MCHitLocation::FT;
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode MCFTDepositMonitor::initialize() {
  StatusCode sc = Consumer::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;

  /// Retrieve and initialize DeFT
  m_deFT = getDet<DeFTDetector>( DeFTDetectorLocation::Default );
  if ( m_deFT == nullptr )
    return Error("Could not initialize DeFTDetector.", StatusCode::FAILURE);
  if( m_deFT->version() < 61 )
    return Error("This version requires FTDet v6.1 or higher", StatusCode::FAILURE);

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================

void MCFTDepositMonitor::operator()(const LHCb::MCFTDeposits& deposits) const {

  // Count signal, noise and spillover deposits
  int nSignal = 0, nNoise = 0, nSpillover = 0;

  // retrieve FTDeposits
  for( auto deposit : deposits ) {

    // Fill the histograms for all types
    std::string hitType("");
    fillHistograms(deposit,hitType);

    // Check the spill type for this contribution to the deposit
    const auto& mcHit  = deposit->mcHit();
    if( mcHit == nullptr ) {
      hitType = "Noise/";
      ++nNoise;
    } else if( mcHit->parent()->registry()->identifier() == FTMCHitLocation ) {
      hitType = deposit->isReflected() ? "SignalRefl/" : "SignalDirect/";
      ++nSignal;
    } else {
      hitType = "Spillover/";
      ++nSpillover;
    }
    fillHistograms(deposit, hitType);
  }

  float maxRange = 1.5e6;
  plot( deposits.size(), "nDeposits",
      "Number of deposits; Deposits/event; Events", 0.,maxRange, 200);
  plot( nSignal, "nSignalDeposits",
      "Number of signal deposits; Signal deposits/event; Events", 0.,maxRange, 200);
  plot( nSpillover, "nSpilloverDeposits",
      "Number of spillover deposits; Spillover deposits/event; Events", 0.,maxRange, 200);
  plot( nNoise, "nNoiseDeposits",
      "Number of noise deposits; Noise deposits/event; Events", 0.,maxRange, 200);

  return;
}

void MCFTDepositMonitor::fillHistograms(const LHCb::MCFTDeposit* deposit,
                                        const std::string& hitType) const {

  const auto photons= deposit->nPhotons();
  LHCb::FTChannelID chanID = deposit->channelID();
  const DeFTModule* module = m_deFT->findModule( chanID );

  // Plot number of photons
  plot(photons, hitType+"nPhotonsPerDeposit",
      "Photons per deposit; Photons/deposit; Deposits" ,
      -0.5, 29.5, 30);

  // plot arrival times
  plot(deposit->time(), hitType+"ArrivalTimePerPhoton",
      "Arrival time of photons; #it{t} [ns]; Photons",
      -25., 150., 100, photons);

  // plot occupancy
  plot((float)chanID.station(), hitType+"PhotonsPerStation",
      "Photons per station; Station; Photons", 0.5 , 3.5, 3, photons);
  plot((float)chanID.module(), hitType+"PhotonsPerModule",
      "Photons per module; Module; Photons", -0.5 , 5.5, 6, photons);
  plot((float)chanID.sipmInModule(), "PhotonsPerSiPM",
      "Photons per SiPM; SiPMID; Photons", -0.5, 15.5, 16, photons);
  plot((float)chanID.channel(), hitType+"PhotonsPerChannel",
      "Photons per channel; Channel; Photons", -0.5, 127.5, 128, photons);
  if ( module != nullptr ) {
    int pseudoChannel = module->pseudoChannel( chanID );
    plot(pseudoChannel, hitType+"PhotonsPerPseudoChannel",
        "Photons per pseudo channel;Pseudo channel;Photons/(64 channels)",
        0., 12288., 192, photons);
  }

}

//=============================================================================
