#ifndef FTCLUSTERMONITOR_H 
#define FTCLUSTERMONITOR_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiAlg/Consumer.h"
#include "GaudiKernel/SystemOfUnits.h"

// from Linker
#include "Associators/Associators.h"

// FTDet
#include "FTDet/DeFTDetector.h"

// LHCbKernel
#include "Kernel/FTChannelID.h"

// from FTEvent
#include "Event/FTLiteCluster.h"
#include "Event/FTCluster.h"

// from MCEvent
#include "Event/MCHit.h"

/** @class FTClusterMonitor FTClusterMonitor.h
 *  
 *
 *  @author Eric Cogneras
 *  @date   2012-07-05
 */

typedef FastClusterContainer<LHCb::FTLiteCluster,int> FTLiteClusters;

class FTClusterMonitor : public Gaudi::Functional::Consumer
                           <void(const FTLiteClusters&, 
                            const LHCb::FTClusters&,
                            const LHCb::MCHits&,
                            const LHCb::LinksByKey& ),
                           Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg> > {
public: 

  FTClusterMonitor(const std::string& name,ISvcLocator* pSvcLocator);

  StatusCode initialize() override;
  StatusCode finalize() override;
  void operator()(const FTLiteClusters&, const LHCb::FTClusters&, 
          const LHCb::MCHits& mcHits, const LHCb::LinksByKey& links) const override;

private:

  Gaudi::Property<float> m_minPforResolution{ this, "MinPforResolution",
    0.0*Gaudi::Units::GeV, "Minimum momentum for resolution plots"};

  DeFTDetector* m_deFT = nullptr; ///< pointer to FT detector description

};
#endif // FTCLUSTERMONITOR_H
