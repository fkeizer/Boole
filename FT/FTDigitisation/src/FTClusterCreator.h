#ifndef FTCLUSTERCREATOR_H 
#define FTCLUSTERCREATOR_H 1

// from Gaudi
#include "GaudiAlg/Transformer.h"

// from Linker
#include "Associators/Associators.h"

// LHCbKernel
#include "Kernel/FTChannelID.h"

// from FTEvent
#include "Event/MCFTDigit.h"
#include "Event/FTLiteCluster.h"
#include "Event/FTCluster.h"

/** @class FTClusterCreator FTClusterCreator.h
 *  
 *
 *  @author Eric Cogneras
 *  @date   2012-04-06
 */

typedef FastClusterContainer<LHCb::FTLiteCluster,int> FTLiteClusters;

class FTClusterCreator : public Gaudi::Functional::MultiTransformer
                           <std::tuple<FTLiteClusters,LHCb::FTClusters,
                           LHCb::LinksByKey,LHCb::LinksByKey,
                           LHCb::LinksByKey,LHCb::LinksByKey>(const LHCb::MCFTDigits& )> {
public: 

  FTClusterCreator(const std::string& name,ISvcLocator* pSvcLocator);

  std::tuple<FTLiteClusters,LHCb::FTClusters,LHCb::LinksByKey,LHCb::LinksByKey,
      LHCb::LinksByKey,LHCb::LinksByKey> operator()(const LHCb::MCFTDigits& digits) const override;

private:

  // Job options

  Gaudi::Property<bool> m_writeFullClusters{ this, "WriteFullClusters",
    false, "Flag to also write out FTClusters" };
  Gaudi::Property<bool> m_storePECharge{ this, "StorePECharge",
    true, "Flag to store PE instead of ADC in FTCluster" };

  Gaudi::Property<unsigned int> m_clusterMinWidth{ this, "ClusterMinWidth", 1,
    "Minimal cluster width"};
  Gaudi::Property<unsigned int> m_clusterMaxWidth{ this, "ClusterMaxWidth", 4,
    "Maximal cluster width"};
  Gaudi::Property<double> m_lowestFraction{ this, "LowestFraction", -0.250,
    "The fraction is defined in the range (-0.250,0.750)"};
  Gaudi::Property<unsigned int> m_largeClusterSize{ this, "LargeClusterSize", 4,
    "Define when to flag unfragmented cluster as large"};

  Gaudi::Property<bool> m_usePEnotADC{ this,"UsePENotADC", false,
    "Flag to use (float)PE instead of (int)ADC"};
  Gaudi::Property<bool> m_fragBigCluster{ this,"FragBigCluster", true,
    "Flag to fragment clusters in ClusterMaxWidth chunks"};
  Gaudi::Property<bool> m_keepBigCluster{ this,"KeepBigCluster", true,
    "Flag to keep (fragmented or not) big clusters"};
  Gaudi::Property<float> m_adcThreshold1{ this, "ADCThreshold1", 1,
    "add-to-cluster threshold"};
  Gaudi::Property<float> m_adcThreshold2{ this, "ADCThreshold2", 2,
    "seed threshold"};
  Gaudi::Property<float> m_adcThreshold3{ this, "ADCThreshold3", 3,
    "single-channel threshold"};
  Gaudi::Property<float> m_adcThreshold1Weight{ this, "ADCThreshold1Weight", 1.5,
    "add-to-cluster weight"};
  Gaudi::Property<float> m_adcThreshold2Weight{ this, "ADCThreshold2Weight", 2.5,
    "seed weight"};
  Gaudi::Property<float> m_adcThreshold3Weight{ this, "ADCThreshold3Weight", 4.5,
    "single-channel weight"};
};
#endif // FTCLUSTERCREATOR_H

