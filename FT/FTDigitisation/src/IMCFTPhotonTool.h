#ifndef IMCFTPHOTONTOOL_H
#define IMCFTPHOTONTOOL_H 1

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

/** @class IMCFTPhotonTool IMCFTPhotonTool.h
 *  
 *  Interface for the tool that transforms deposited energies to photons exiting
 *  the fibre ends
 *
 *  @author Violaine Bellee, Julian Wishahi
 *  @date   2017-02-14
 */

struct IMCFTPhotonTool : extend_interfaces<  IAlgTool > {

  // Return the interface ID
  DeclareInterfaceID ( IMCFTPhotonTool, 1, 0 );
 
  virtual double numExpectedPhotons(double effective_energy) = 0;
  virtual int numObservedPhotons(double num_expected_photons) = 0;
  virtual double averagePropagationTime(double distToSiPM) = 0;
  virtual double generateScintillationTime() = 0;
  virtual void   generatePhoton(double& time, double& wavelength,
  								double& posX, double& posZ, 
  								double& dXdY, double& dZdY) = 0;
 
};
 
#endif // IMCFTPHOTONTOOL_H
