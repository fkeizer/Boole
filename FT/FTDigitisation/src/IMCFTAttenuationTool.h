#ifndef IMCFTATTENUATIONTOOL_H 
#define IMCFTATTENUATIONTOOL_H 1

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

/** @class IMCFTAttenuationTool IMCFTAttenuationTool.h
 *  
 *  Interface for the tool that calculates the light attenuation for the MCFTDepositCreator
 *
 *  @author Michel De Cian
 *  @date   2015-01-15
 */

struct IMCFTAttenuationTool : extend_interfaces<  IAlgTool > {

  // Return the interface ID
  DeclareInterfaceID ( IMCFTAttenuationTool, 1, 0 );

  virtual void attenuation(double x, double y, double& att, double& attRef) = 0;
  
};
#endif // IMCFTATTENUATIONTOOL_H
