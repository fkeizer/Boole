#ifndef MCFTDISTRIBUTIONCHANNELTOOL_H 
#define MCFTDISTRIBUTIONCHANNELTOOL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/RndmGenerators.h"

// from local
#include "IMCFTDistributionChannelTool.h"            // Interface

/** @class MCFTDistributionChannelTool MCFTDistributionChannelTool.h
 *  
 *  This tool calculates the fraction of the total path length
 *  of a track in the core of each fibre that was hit along with
 *  the position of the fibre.
 *
 *  @author Tobias Tekampe
 *  @author based on Julian Wishahi's MCSciFiEnergyInFibreCoreCalc
 *  @date   2015-10-27
 */


class MCFTDistributionChannelTool : public extends<GaudiTool,IMCFTDistributionChannelTool> {

public: 

  using base_class::base_class;

  StatusCode initialize() override;
  
  LHCb::FTChannelID targetChannel(double posX, const DeFTMat& mat) override;
  LHCb::FTChannelID targetChannel(double posX, double posZ,
                                  double dXdY, double dZdY,
                                  const DeFTMat& mat) override;
  
  FTChannelIDsAndFracs targetChannelsFractions(double posXentry,
                                               double posXexit,
                                               const DeFTMat& mat) override;

private:
  /// Calculate the final x position of a photon based on its slope at the fibre
  inline double findFinalPosSlope(double pos, double slope) const {
    return pos + m_epoxyWidth * slope;
  }

  /// Bind function to one of the implementations below
  std::function<double(double)> m_getProbFraction;

  /// Calculate the fraction of energy deposited for a given relative
  /// position (-0.5,0.5)
  inline double probFracGaussSharing(double frac) const {
    const double erf_arg = (std::abs(frac) - 0.5) /
                            (std::sqrt(2.) * m_sigmaPhotonXDistribution );
    return 0.5*(1.0-std::erf(erf_arg));
  }

  /// Calculate the fraction of energy deposited for a given relative
  /// position (-0.5,0.5)
  inline double probFracOldSharing(double frac) const {
    frac = std::abs(frac);
    int i = int(2.0*frac);
    return ( i < 3 ) ?
        m_lsNots[i] + (m_lsNots[i+1] - m_lsNots[i]) * (2.0*frac - i) : 0.0;
  }

  /// Calculate the fraction of energy deposited for a given relative
  /// position (-0.5,0.5)
  inline double probFracNoSharing(double frac) const {
    return (std::abs(frac) < 0.5) ? 1.0 : 0.0;
  }


  Gaudi::Property<std::string> m_lightSharing{this, "LightSharing", "gauss",
      "possible values: gauss, old, no"};

  // members for detailed simulation
  Gaudi::Property<double> m_channelSizeZ{ this,
    "ChannelSizeZ", 1.625*Gaudi::Units::mm,
    "Channel width in z direction (local coordinates)"};
  Gaudi::Property<double> m_epoxyWidth{ this,
    "EpoxyWidth", 0.1*Gaudi::Units::mm,
    "Thickness of the epoxy layer between fibre ends and SiPMs"};

  // members for effective simulation
  Gaudi::Property<double> m_minEnergyFraction{ this,
    "MinEnergyFraction", 0.005,
    "Minimum energy fraction to create a signal deposit"};
  Gaudi::Property<float> m_packingFactor{ this,
    "PackingFactor", 0.64, ///< Default value: (6*0.11^2*pi/(0.275*1.3)=0.64
    "Packing factor of the core of the fibres in the mat"};
  Gaudi::Property<double> m_sigmaPhotonXDistribution{this,
    "GaussianSharingWidth", 0.33,
    "Width of Gaussian to distribute photons to channels"
    " (in units of channel pitch)."};
  Gaudi::Property<double> m_lsC{ this, "OldLightSharingCentral", 0.68,
    "Light sharing for a central photon"};
  Gaudi::Property<double> m_lsE{ this, "OldLightSharingEdge", 0.5,
    "Light sharing for a photon on the edge"};
  /// Nots used for old light-sharing function at 0.5 spacing
  std::array<double,4> m_lsNots{{ m_lsC, m_lsE, 0.5-0.5*m_lsC, 1.0-2.0*m_lsE }};

  /// Number of additional channels to consider
  unsigned int m_numOfAdditionalChannels = 2;

};


#endif // MCFTDISTRIBUTIONCHANNELTOOL_H
