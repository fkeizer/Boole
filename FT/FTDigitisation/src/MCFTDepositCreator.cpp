/** @file MCFTDepositCreator.cpp
 *
 *  Implementation of class : MCFTDepositCreator
 *
 *  @author DE VRIES Jacco, WISHAHI Julian, BELLEE Violaine, COGNERAS Eric, PESCATORE Luca
 *  @date   2016-12-19
 */

// external libs
#include <range/v3/all.hpp>

// local
#include "MCFTDepositCreator.h"

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( MCFTDepositCreator )

MCFTDepositCreator::MCFTDepositCreator(const std::string& name,ISvcLocator* pSvcLocator) :
    MultiTransformer(name, pSvcLocator,
            // Input
            { KeyValue{ "InputLocation", "/Event/MC/FT/MergedHits"} },
            // Output
            { KeyValue{"OutputLocation",      LHCb::MCFTDepositLocation::Default},
              KeyValue{"MCFTPhotonsLocation", LHCb::MCFTPhotonLocation::Default} } ) {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode MCFTDepositCreator::initialize() {
  StatusCode sc = MultiTransformer::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  

  // check type of chosen simulation
  auto simulationTypePair = m_simulationTypes.find(m_simulationTypeChoice);
  if (simulationTypePair != m_simulationTypes.end()){
    m_simulationType = simulationTypePair->second;
  } else {
    std::string error_message("Simulation type "+m_simulationTypeChoice+
        " is unknown. Please choose from: ");
    for (auto simulationType : m_simulationTypes){
      error_message += simulationType.first + " ";
    }

    return Error(error_message, StatusCode::FAILURE);
  }

  // Retrieve and initialize DeFT
  m_deFT = getDet<DeFTDetector>( DeFTDetectorLocation::Default );
  if ( m_deFT == nullptr )
    return Error("Could not initialize DeFTDetector.", StatusCode::FAILURE);
  if( m_deFT->version() < 61 )
    return Error("This version requires FTDet v6.1 or higher", StatusCode::FAILURE);

  // AttenuationTool
  m_attenuationTool = tool<IMCFTAttenuationTool>( m_attenuationToolName, this );
  if( m_attenuationTool == nullptr )
    return Error("Could not find: "+ m_attenuationToolName, StatusCode::FAILURE);

  // DistributionFibreTool
  m_distrFibreTool = tool<IMCFTDistributionFibreTool>( m_distrFibreToolName, this );
  if( m_distrFibreTool == nullptr )
    return Error("Could not find: "+ m_distrFibreToolName, StatusCode::FAILURE);

  // PhotonTool
  m_photonTool = tool<IMCFTPhotonTool>( m_photonToolName, this );
  if( m_photonTool == nullptr )
    return Error("Could not find: "+ m_photonToolName, StatusCode::FAILURE);

  // DistributionChannelTool
  m_distrChannelTool = tool<IMCFTDistributionChannelTool>( m_distrChannelToolName, this );
  if( m_distrChannelTool == nullptr )
    return Error("Could not find: "+ m_distrChannelToolName, StatusCode::FAILURE);

  // SiPmTool
  m_sipmTool = tool<IFTSiPMTool>( m_sipmToolName, this );
  if( m_sipmTool == nullptr )
    return Error("Could not find: "+ m_sipmToolName, StatusCode::FAILURE);

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
std::tuple<LHCb::MCFTDeposits,LHCb::MCFTPhotons> MCFTDepositCreator::operator()(
    const std::array<SpillPair,4>& spills) const {

  std::tuple<LHCb::MCFTDeposits,LHCb::MCFTPhotons> returnContainers;
  auto& deposits = std::get<0>(returnContainers);
  auto& photons = std::get<1>(returnContainers);

  // main loop over spills and MCHits
  switch(m_simulationType){
  case SimulationType::detailed:
    photons.reserve(10e5);  // reserve 1.0M elements
    deposits.reserve(14e5); // reserve 1.4M elements
    break;
  case SimulationType::effective:
    photons.reserve(0);     // reserve zero elements
    deposits.reserve(10e5); // reserve 1.0M elements
    break;
  }

  // main loop over spills and MCHits
  for( const auto& spill : spills ){
    const LHCb::MCHits* mchits = spill.second;

    // Check if spill is missing
    if (mchits == nullptr) {
      if (msgLevel(MSG::DEBUG)) debug() << "Spillover missing in the loop at "
                                        << spill.first << " ns" << endmsg;
      continue;
    }
    counter("NbOfMCHits")+= mchits->size();

    for( const LHCb::MCHit* mchit : *mchits ){

      // get sensDetID of MCHit
      const int sensDetID = mchit->sensDetID();

      // Check if sensDetID is filled and if a mat can be associated
      if (sensDetID == -1) {
        counter("NbOfMissedMCHits")++;
        debug() << "Empty sensDetID of MCHit (happens <1 times/event)" << endmsg;
        continue;
      }
      const DeFTMat* mat_ptr = m_deFT->findMat(sensDetID);
      if ( mat_ptr == nullptr ) {
        error() << "FT module not found for sensDetID = " << sensDetID << endmsg;
        return returnContainers;
      }
      const DeFTMat& mat = *mat_ptr;
      // choose which function to call depending on simulation type
      switch(m_simulationType){
      case SimulationType::detailed:
        convertHitToPhotons(mchit, mat, spill.first, photons);
        break;
      case SimulationType::effective:
        convertHitToDepositsEffective(mchit, mat, spill.first, deposits);
        break;
      }
    } // loop on hits
  } // loop on spills

  // Finally, sort the photon container according to mat ID
  std::stable_sort( photons.begin(), photons.end(), LHCb::MCFTPhoton::lowerByMatID );

  // For detailed simulation loop over photons is required
  switch(m_simulationType){
  case SimulationType::detailed:
    convertPhotonsToDepositsDetailed(photons, deposits);
    break;
  case SimulationType::effective:
    break;
  }
  if (m_simulateNoise) m_sipmTool->addNoise(&deposits);

  // Finally, sort the deposit container according to channel ID
  std::stable_sort( deposits.begin(), deposits.end(),
                    LHCb::MCFTDeposit::lowerByChannelID );

  if ( msgLevel( MSG::DEBUG) )
    debug() << "Number of MCFTDeposits created: " << deposits.size()<< endmsg;

  return returnContainers;
}

//=============================================================================
// Detailed simulation
//=============================================================================
void MCFTDepositCreator::convertHitToPhotons(const LHCb::MCHit* mchit,
                                             const DeFTMat& mat,
                                             double timeOffset,
                                             LHCb::MCFTPhotons& photons) const
{
  // Get MCFTPhotons for mat or create it
  const unsigned int sensDetID = mat.elementID();

  // Get deposited energy
  const double hitEnergy = mchit->energy();

  // Get the local coordinates
  const Gaudi::XYZPoint  localEntry = mat.geometry()->toLocal(mchit->entry());
  const Gaudi::XYZVector localDispl = mat.geometry()->toLocal(mchit->displacement());

  const Gaudi::XYZPoint localExit  = localEntry+localDispl;
  const Gaudi::XYZPoint localMid   = localEntry+0.5*localDispl;

  // -- Calculate the attenuation using the AttenuationTool
  double attDir = 1., attRef = 1.;
  m_attenuationTool->attenuation( mchit->midPoint().X(), mchit->midPoint().Y(),
      attDir, attRef);

  // Calculate fibre centers and path fraction in the fibres hit by the MCHit
  auto posAndFracs = m_distrFibreTool->effectivePathFracInCores( localEntry, localExit );

  // -- Calculate the minimal arrival time
  const double timeEvent = mchit->time() + timeOffset;
  const double distDir   = mat.distanceToSiPM(localMid);
  const double distRef   = 2.*mat.fibreLength() - distDir;

  const double propTimeDirMean = timeEvent + m_photonTool->averagePropagationTime(distDir);
  const double propTimeRefMean = timeEvent + m_photonTool->averagePropagationTime(distRef);

  for( auto posAndFrac : posAndFracs ){
    // get fibre position and deposited energy
    double posXFibre = posAndFrac.first.X();
    double posZFibre = posAndFrac.first.Z();
    double energyDir = posAndFrac.second * hitEnergy * attDir;
    double energyRef = posAndFrac.second * hitEnergy * attRef;

    // get number of expected photons
    double nPhotonsExpDir = m_photonTool->numExpectedPhotons(energyDir);
    double nPhotonsExpRef = m_photonTool->numExpectedPhotons(energyRef);

    switch(m_simulationType){
      case SimulationType::detailed: {
        int nPhotonsDir = m_photonTool->numObservedPhotons(nPhotonsExpDir);
        int nPhotonsRef = m_photonTool->numObservedPhotons(nPhotonsExpRef);

        // create direct photons
        for (int i = 0; i < nPhotonsDir; ++i) {
          double time, wavelength, posX, posZ, dXdY,dZdY;
          m_photonTool->generatePhoton(time,wavelength,posX,posZ,dXdY,dZdY);
          LHCb::MCFTPhoton* photon = new LHCb::MCFTPhoton(sensDetID, 1.,
                                                          mchit,
                                                          propTimeDirMean+time,
                                                          wavelength,
                                                          posXFibre+posX,
                                                          posZFibre+posZ,
                                                          dXdY,dZdY,false);
          if(!m_simulatePDE || m_sipmTool->sipmDetectsPhoton(wavelength))
          photons.add( photon );
        }
        
        // create reflected photons
        for (int i = 0; i < nPhotonsRef; ++i) {
          double time, wavelength, posX, posZ, dXdY,dZdY;
          m_photonTool->generatePhoton(time,wavelength,posX,posZ,dXdY,dZdY);
          LHCb::MCFTPhoton* photon = new LHCb::MCFTPhoton(sensDetID, 1.,
                                                          mchit,
                                                          propTimeRefMean+time,
                                                          wavelength,
                                                          posXFibre+posX,
                                                          posZFibre+posZ,
                                                          dXdY, dZdY,true);

          if(!m_simulatePDE || m_sipmTool->sipmDetectsPhoton(wavelength))
          photons.add( photon );
        }
    }
    break;
    case SimulationType::effective:
      error() << "Calling function for detailed simulation, "
      << "but simulation type is 'effective'!" << endmsg;
      break;
    }
  }
}

void MCFTDepositCreator::convertPhotonsToDepositsDetailed(const LHCb::MCFTPhotons& photons,
                                                          LHCb::MCFTDeposits& deposits) const
{
  int prevSensDetID = -1000;
  const DeFTMat* mat_ptr = nullptr;
  for( const auto& photon : photons ){
    if( photon->sensDetID() != prevSensDetID ) {
      prevSensDetID = photon->sensDetID();
      mat_ptr = m_deFT->findMat( prevSensDetID );
      if( mat_ptr == nullptr ) {
        error() << "FT module not found for sensDetID = " << photon->sensDetID() << endmsg;
        continue;
      }
    }

    // Find the associated channel, add it to the deposit
    LHCb::FTChannelID targetChannel =
        m_distrChannelTool->targetChannel( photon->posX(), photon->posZ(),
                                           photon->dXdY(), photon->dZdY(), *mat_ptr );
    if (targetChannel.channelID() == 0u) continue;

    // Add direct x-talk
    int nPhotonsXTalk = (m_simulateIntraChannelXTalk) ?
        m_sipmTool->generateDirectXTalk( photon->nPhotons() ) : 0;

    LHCb::MCFTDeposit* deposit =  new LHCb::MCFTDeposit(targetChannel, photon->mcHit(),
        photon->nPhotons() + nPhotonsXTalk, photon->time(), photon->isReflected()  );
    deposits.add( deposit );

    // Generate delayed XTalk
    if( m_simulateIntraChannelXTalk ) {
      int nDelayedXtalk = m_sipmTool->generateDelayedXTalk( photon->nPhotons() );
      for( int i=0; i<nDelayedXtalk; ++i ) {
        float addTime =  m_sipmTool->generateDelayedXTalkTime();
        LHCb::MCFTDeposit* deposit = new LHCb::MCFTDeposit(targetChannel,
            photon->mcHit(), 1, photon->time() + addTime, photon->isReflected()  );
        deposits.add( deposit );
      }
    }
  } // loop over photons
  if ( msgLevel( MSG::DEBUG) )
    debug() << "Number of deposits after looping over photons = "
            << deposits.size() << endmsg;
}

//=============================================================================
// Effective simulation
//=============================================================================
void MCFTDepositCreator::convertHitToDepositsEffective(const LHCb::MCHit* mchit, 
                                                       const DeFTMat& mat,
                                                       double timeOffset,
                                                       LHCb::MCFTDeposits& deposits) const
{
  // Get deposited energy
  const double hitEnergy = mchit->energy();

  // Get the local coordinates
  const Gaudi::XYZPoint  localEntry = mat.geometry()->toLocal(mchit->entry());
  const Gaudi::XYZVector localDispl = mat.geometry()->toLocal(mchit->displacement());

  const Gaudi::XYZPoint localExit  = localEntry+localDispl;
  const Gaudi::XYZPoint localMid   = localEntry+0.5*localDispl;

  // -- Calculate the attenuation using the AttenuationTool
  double attDir = 1., attRef = 1.;
  m_attenuationTool->attenuation( mchit->midPoint().X(), mchit->midPoint().Y(),
      attDir, attRef );

  // -- Calculate the minimal arrival time
  const double timeEvent  = mchit->time() + timeOffset;
  const double distDir = mat.distanceToSiPM(localMid);
  const double distRef = 2.*mat.fibreLength() - distDir;

  // -- Get average propagation times
  const double propTimeDirMean = timeEvent + m_photonTool->averagePropagationTime(distDir);
  const double propTimeRefMean = timeEvent + m_photonTool->averagePropagationTime(distRef);

  // Get channels and fractions
  auto channelsAndFracs =
      m_distrChannelTool->targetChannelsFractions(localEntry.X(), localExit.X(), mat);

  // Time observables
  double timeScint = m_photonTool->generateScintillationTime();
  double timeDir = timeScint + propTimeDirMean;
  double timeRef = timeScint + propTimeRefMean;

  for (auto channelAndFrac : channelsAndFracs){
    bool isReflected = false;
    for( const auto& pair : { std::make_pair(attDir,timeDir),
                              std::make_pair(attRef,timeRef) } ) {
      // calculate fractional energy
      double energy = hitEnergy * channelAndFrac.second * pair.first;

      // get number of expected photons
      double nPhotonsExp = m_photonTool->numExpectedPhotons(energy);

      // get number of observed photons
      int nPhotons = m_photonTool->numObservedPhotons(nPhotonsExp);

      // simulate direct cross-talk
      unsigned int nPhotonsXTalk=0;
      if ( m_simulateIntraChannelXTalk )
        nPhotonsXTalk = m_sipmTool->generateDirectXTalk(nPhotons);

      auto deposit = new LHCb::MCFTDeposit(channelAndFrac.first,
          mchit, nPhotons+nPhotonsXTalk, pair.second, isReflected );
      deposits.add( deposit );

      // Generate delayed XTalk
      if( m_simulateIntraChannelXTalk ) {
        int nDelayedXtalk = m_sipmTool->generateDelayedXTalk( nPhotons );
        for( int i=0; i<nDelayedXtalk; ++i ) {
          float addTime =  m_sipmTool->generateDelayedXTalkTime();
          auto deposit = new LHCb::MCFTDeposit(channelAndFrac.first,
              mchit, 1, pair.second + addTime, isReflected );
          deposits.add( deposit );
        }
      }

      isReflected = true; // set to true for 2nd iteration
    }
  }
}

