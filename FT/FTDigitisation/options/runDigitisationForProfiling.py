# Author: Julian Wishahi
# Date: 2016-09-23
# Description: Boole options to profile the digitisation. 
#   Run by calling
#     ./run gaudirun.py --profilerName=valgrindcallgrind --profilerExtraOptions="__instr-atstart=no -v __smc-check=all-non-file __dump-instr=yes __trace-jump=yes __callgrind-out-file=callgrind.improved_gauss.out" runDigitisationForProfiling.py inputfiles.py
#   from a BooleDev directory (assuming lb-dev etc.)

from Gaudi.Configuration import *
from Configurables import Boole, LHCbApp, DDDBConf, CondDB

LHCbApp().Simulation = True
CondDB().Upgrade = True

Boole().DDDBtag = 'dddb-20160304'
Boole().CondDBtag = 'sim-20150716-vc-md100'

from Configurables import CondDB
CondDB().addLayer(dbFile = "DDDB_FT61.db", dbName = "DDDB")

Boole().DataType   = "Upgrade"

Boole().DetectorDigi = ['VP', 'UT', 'FT', 'Magnet']
Boole().DigiSequence = ['VP', 'UT', 'FT']
Boole().DetectorLink = ['VP', 'UT', 'Tr', 'FT', 'Magnet']
Boole().DetectorMoni = []
Boole().EvtMax = 20

EventSelector().Input = ["DATAFILE='root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Custom_Geoms_Upgrade/samples/UFT_61/Gauss-30000000-100ev-noSpillover-20161026.sim?svcClass=default' TYP='POOL_ROOTTREE' OPT='READ'"]


#===============================================================================
# Configure FT Digitization
#===============================================================================

#-------------------------------------------------------------------------------
# MCFTDepositCreator
#-------------------------------------------------------------------------------
from Configurables import MCFTDepositCreator, MCFTDistributionChannelTool

myAlgDeposit = MCFTDepositCreator()
myAlgDeposit.SimulationType = "detailed" # detailed, improved, effective
myAlgDeposit.SimulateNoise = True
myAlgDeposit.SimulateIntraChannelXTalk = True

distrChanTool = myAlgDeposit.addTool(MCFTDistributionChannelTool())
distrChanTool.LightSharing = "old" # no, old, gauss

#-------------------------------------------------------------------------------
# MCFTDigitCreator
#-------------------------------------------------------------------------------
from Configurables import MCFTDigitCreator
myAlgDigit = MCFTDigitCreator()

#-------------------------------------------------------------------------------
# FTClusterCreator
#-------------------------------------------------------------------------------
from Configurables import FTClusterCreator

myAlgCluster = FTClusterCreator()
myAlgCluster.WriteFullClusters = True

#-------------------------------------------------------------------------------
# FTRawBankEncoder
#-------------------------------------------------------------------------------
from Configurables import FTRawBankEncoder
myAlgRawBankEncoder = FTRawBankEncoder()

#-------------------------------------------------------------------------------
# MCFTDepositMonitor
#-------------------------------------------------------------------------------
from Configurables import MCFTDepositMonitor
def removeMonitors():
	GaudiSequencer("DigiFTSeq").Members.remove("MCFTDepositMonitor")

from Gaudi.Configuration import appendPostConfigAction
appendPostConfigAction( removeMonitors )

#===============================================================================
# Outputname
#===============================================================================
Boole().DatasetName = "DigitisationForProfiling_"+myAlgDeposit.SimulationType+'_'+distrChanTool.LightSharing

#===============================================================================
# Profiling
#===============================================================================
def addProfile():
    from Configurables import CallgrindProfile
    p = CallgrindProfile('CallgrindProfile')
    p.StartFromEventN = 5
    p.StopAtEventN = 15
    p.DumpAtEventN = 16
    p.DumpName = 'callgrind_'+myAlgDeposit.SimulationType+'_'+distrChanTool.LightSharing+'.out'
    GaudiSequencer('DigiFTSeq').Members.insert(0, p)
#appendPostConfigAction(addProfile)
