
//===============================================================================
/** @file RichSignal.cpp
 *
 *  Implementation file for RICH digitisation algorithm : RichSignal
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @author Alex Howard   a.s.howard@ic.ac.uk
 *  @date   2003-11-06
 */
//===============================================================================

#include "RichSignal.h"

using namespace Rich::MC::Digi;

DECLARE_COMPONENT( Signal )

// Standard constructor, initializes variables
Signal::Signal( const std::string& name,
                ISvcLocator* pSvcLocator )
  : Rich::AlgBase  ( name, pSvcLocator ),
    m_timeShift ( Rich::NRiches )
{

  declareProperty( "HitLocation",
                   m_RichHitLocation = LHCb::MCRichHitLocation::Default );
  declareProperty( "PrevLocation",
                   m_RichPrevLocation = "Prev/" + LHCb::MCRichHitLocation::Default );
  declareProperty( "PrevPrevLocation",
                   m_RichPrevPrevLocation = "PrevPrev/" + LHCb::MCRichHitLocation::Default );
  declareProperty( "NextLocation",
                   m_RichNextLocation = "Next/" + LHCb::MCRichHitLocation::Default );
  declareProperty( "NextNextLocation",
                   m_RichNextNextLocation = "NextNext/" + LHCb::MCRichHitLocation::Default );
  declareProperty( "LHCBackgroundLocation",
                   m_lhcBkgLocation = "LHCBackground/" + LHCb::MCRichHitLocation::Default );
  declareProperty( "DepositLocation",
                   m_RichDepositLocation = LHCb::MCRichDepositLocation::Default );

  m_timeShift[Rich::Rich1] = 0;
  m_timeShift[Rich::Rich2] = 40;
  declareProperty( "TimeCalib", m_timeShift );

  declareProperty( "UseSpillover",     m_doSpillover  = true  );
  declareProperty( "UseLHCBackground", m_doLHCBkg     = true  );
  declareProperty( "CheckSmartIDs",    m_testSmartIDs = false );

}

StatusCode Signal::initialize()
{
  // Initialize base class
  auto sc = Rich::AlgBase::initialize();
  if ( sc.isFailure() ) { return sc; }

  // randomn number generator
  sc = m_rndm.initialize( randSvc(), Rndm::Flat(0.,1.) );
  if ( sc.isFailure() )
  {
    return Error( "Unable to initialise random number generator" );
  }

  // tools
  acquireTool( "RichSmartIDTool", m_smartIDTool, nullptr, true );
  acquireTool( "RichMCTruthTool", m_truth, nullptr, true       );

  return sc;
}

StatusCode Signal::execute()
{
  _ri_debug << "Execute" << endmsg;

  // Form new container of MCRichDeposits
  m_mcDeposits = new LHCb::MCRichDeposits();
  put( m_mcDeposits, m_RichDepositLocation );

  // Process main event
  // must be done first (so that first associated hit is signal)
  StatusCode sc = ProcessEvent( m_RichHitLocation, 0, 0 );

  // if requested, process spillover events
  if ( m_doSpillover )
  {
    sc = sc && ProcessEvent( m_RichPrevPrevLocation, -50, -2 );
    sc = sc && ProcessEvent( m_RichPrevLocation,     -25, -1 );
    sc = sc && ProcessEvent( m_RichNextLocation,      25,  1 );
    sc = sc && ProcessEvent( m_RichNextNextLocation,  50,  2 );
  }

  // if requested, process LHC background
  if ( m_doLHCBkg ) { sc = sc && ProcessEvent( m_lhcBkgLocation, 0, 0 ); }

  // Debug Printout
  _ri_debug << "Created overall " << m_mcDeposits->size()
            << " MCRichDeposits at " << m_RichDepositLocation << endmsg;

  return sc;
}

StatusCode Signal::ProcessEvent( const std::string & hitLoc,
                                 const double tofOffset,
                                 const int eventType ) const
{

  // Load hits
  LHCb::MCRichHits * hits = getIfExists<LHCb::MCRichHits>( hitLoc );
  if ( !hits ) return StatusCode::SUCCESS;
  ++counter( "Found MCRichHits at " + hitLoc );
  _ri_debug << "Successfully located " << hits->size()
            << " MCRichHits at " << hitLoc << " : Pointer=" << hits << endmsg;

  unsigned int nDeps(0);
  for ( const auto * hit : *hits )
  {

    // Get RichSmartID from MCRichHit (stripping sub-pixel info for the moment)
    const auto id = hit->sensDetID().pixelID();

    // Run some checks
    if ( UNLIKELY(m_testSmartIDs) )
    {
      LHCb::RichSmartID tempID;
      const auto ok = (m_smartIDTool->smartID(hit->entry(),tempID));
      if      ( !ok )
      {
        Warning( "Failed to compute RichSmartID from MCRichHit entry point" ).ignore();
      }
      else if ( id != tempID.pixelID() )
      {
        Warning( "RichSmartID mis-match. Enable DEBUG for more details." ).ignore();
        if ( msgLevel(MSG::DEBUG) )
        {
          debug() << "Original " << id << endmsg;
          debug() << "New      " << tempID.pixelID() << endmsg;
        }
      }
      if ( msgLevel(MSG::VERBOSE) )
      {
        Gaudi::XYZPoint detectP;
        const StatusCode sc = m_smartIDTool->globalPosition( id, detectP );
        if ( sc.isFailure() )
        {
          Warning( "Problem translating RichSmartID into global position" ).ignore();
        }
        else
        {
          verbose() << id << endmsg;
          verbose() << " -> Hit   pos : " << hit->entry() << endmsg;
          verbose() << " -> Digit pos : " << detectP << endmsg;
        }
      }
    }

    // Create a new deposit
    LHCb::MCRichDeposit*  dep = new LHCb::MCRichDeposit();
    m_mcDeposits->insert( dep );
    ++nDeps;

    // Set RichSmartID
    dep->setSmartID( id );

    // set parent hit
    dep->setParentHit( hit );

    // Hit energy
    dep->setEnergy( hit->energy() );

    // TOF
    const auto tof = tofOffset + hit->timeOfFlight() - m_timeShift[hit->rich()];
    dep->setTime( tof );

    // get history from hit
    auto hist = hit->mcRichDigitHistoryCode();

    // add event type to history
    if      (  0 == eventType ) { hist.setSignalEvent(true);   }
    else if ( -1 == eventType ) { hist.setPrevEvent(true);     }
    else if ( -2 == eventType ) { hist.setPrevPrevEvent(true); }
    else if (  1 == eventType ) { hist.setNextEvent(true);     }
    else if (  2 == eventType ) { hist.setNextNextEvent(true); }

    // Update history in dep
    dep->setHistory( hist );

  } // hit loop

  _ri_debug << "Created " << nDeps << " MCRichDeposits for " << hitLoc << endmsg;

  return StatusCode::SUCCESS;
}

StatusCode Signal::finalize()
{
  // finalize random number generator
  const auto sc = m_rndm.finalize();
  if ( !sc ) Warning( "Failed to finalise random number generator" ).ignore();

  // finalize base class
  return Rich::AlgBase::finalize();
}
