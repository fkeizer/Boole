
//-----------------------------------------------------------------------------
/** @file MCRichHitsToRawBufferAlg.cpp
 *
 *  Implementation file for RICH DAQ algorithm : MCRichHitsToRawBufferAlg
 *
 *  @author Chris Jones  Christopher.Rob.Jones@cern.ch
 *  @date   2003-11-09
 */
//-----------------------------------------------------------------------------

// local
#include "MCRichHitsToRawBufferAlg.h"

// namespaces
using namespace Rich::MC::Digi;

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( MCRichHitsToRawBufferAlg )

// Standard constructor
MCRichHitsToRawBufferAlg::MCRichHitsToRawBufferAlg( const std::string& name,
                                                        ISvcLocator* pSvcLocator )
  : Rich::AlgBase ( name, pSvcLocator )
{
  declareProperty( "MCRichHitsLocation",
                   m_hitsLoc = LHCb::MCRichHitLocation::Default );
  declareProperty( "DataVersion", m_version = Rich::DAQ::LHCb2 );
}

// Initialisation.
StatusCode MCRichHitsToRawBufferAlg::initialize()
{

  // intialise base lcass
  const StatusCode sc = Rich::AlgBase::initialize();
  if ( sc.isFailure() ) { return sc; }

  // acquire tools
  acquireTool( "RichRawDataFormatTool", m_rawFormatT, nullptr, true );

  info() << "Using RICH Level1 buffer format : " << m_version << endmsg;

  return sc;
}

// Main execution
StatusCode MCRichHitsToRawBufferAlg::execute()
{

  // Retrieve MCRichHits
  const LHCb::MCRichHits * hits = get<LHCb::MCRichHits>( m_hitsLoc );

  // new vector of smart IDs
  LHCb::RichSmartID::Vector smartIDs;
  smartIDs.reserve( hits->size() );

  // Loop over hits and fill smartIDs into vector
  for ( const auto * hit : *hits )
  {
    smartIDs.push_back( hit->sensDetID() );
  }

  // Fill raw buffer
  m_rawFormatT->fillRawEvent( smartIDs, (Rich::DAQ::BankVersion)m_version );

  return StatusCode::SUCCESS;
}
