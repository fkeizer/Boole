
//===============================================================================
/** @file RichDetailedFrontEndResponse.cpp
 *
 *  Implementation file for RICH digitisation algorithm : RichDetailedFrontEndResponse
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @author Alex Howard   a.s.howard@ic.ac.uk
 *  @date   2003-11-06
 */
//===============================================================================

#include "RichDetailedFrontEndResponse.h"

using namespace Rich::MC::Digi;

DECLARE_COMPONENT( DetailedFrontEndResponse )

// Standard constructor, initializes variables
DetailedFrontEndResponse::DetailedFrontEndResponse( const std::string& name,
                                                    ISvcLocator* pSvcLocator)
: Rich::AlgBase ( name, pSvcLocator ),
  m_timeShift   ( Rich::NRiches )
{

  // job options
  declareProperty( "MCRichSummedDepositsLocation",
                   m_mcRichSummedDepositsLocation = LHCb::MCRichSummedDepositLocation::Default );
  declareProperty( "MCRichDigitsLocation",
                   m_mcRichDigitsLocation = LHCb::MCRichDigitLocation::Default );
  //  declareProperty( "SimpleCalibration", m_Calibration = 12500. );
  declareProperty( "SimpleCalibration", m_Calibration = 8330. );
  declareProperty( "SimpleBaseline",    m_Pedestal = 50 );
  declareProperty( "Noise",             m_Noise = 150. );  // in electrons
  declareProperty( "Threshold",         m_Threshold = 1400. ); // in electrons
  declareProperty( "ThresholdSigma",    m_ThresholdSigma = 140. ); // in electrons

  // Shift time ( value correlated settings in RichSignal )
  m_timeShift[Rich::Rich1] = 10;
  m_timeShift[Rich::Rich2] = 6;
  declareProperty( "TimeCalib", m_timeShift );

  el_per_adc = 40.;

  Rndm::Numbers m_gaussThreshold;
  Rndm::Numbers m_gaussNoise; // currently hardwired to be 150 electrons (sigma) hack

}

StatusCode DetailedFrontEndResponse::initialize()
{
  // Initialize base class
  StatusCode sc = Rich::AlgBase::initialize();
  if ( sc.isFailure() ) { return sc; }

  // create a collection of all pixels
  const Rich::ISmartIDTool * smartIDs(nullptr);
  acquireTool( "RichSmartIDTool" , smartIDs, nullptr, true );
  const LHCb::RichSmartID::Vector & pixels = smartIDs->readoutChannelList();
  if ( msgLevel(MSG::DEBUG) )
    debug() << "Retrieved " << pixels.size() << " pixels in active list" << endmsg;
  theRegistry = new RichRegistry();
  actual_base = theRegistry->GetNewBase( pixels );
  if ( !actual_base )
  {
    return Error( "Failed to intialise pixel list" );
  }

  // initialise random number generators
  sc = ( m_gaussNoise.initialize     ( randSvc(), Rndm::Gauss(0., m_Noise)                  ) &&
         m_gaussThreshold.initialize ( randSvc(), Rndm::Gauss(m_Threshold,m_ThresholdSigma) ) );
  if ( sc.isFailure() ) { return Error( "Failed to initialise random number generators", sc ); }

  releaseTool( smartIDs );

  info() << "Time Calibration (Rich1/RICH2) = " << m_timeShift << endmsg;

  return sc;
}

StatusCode DetailedFrontEndResponse::finalize()
{
  // clean up
  delete actual_base;
  //delete theRegistry; // CRJ :  Disabled since causes a crash. For Marco A. to fix ;)

  // finalise random number generators
  const StatusCode sc = ( m_gaussNoise.finalize() && m_gaussThreshold.finalize() );
  if ( sc.isFailure() ) { Warning( "Failed to finalise random number generators" ).ignore(); }

  // finalize base class
  return Rich::AlgBase::finalize();
}

StatusCode DetailedFrontEndResponse::execute()
{
  _ri_debug << "Execute" << endmsg;

  m_summedDeposits = get<LHCb::MCRichSummedDeposits>( m_mcRichSummedDepositsLocation );
  _ri_debug << "Successfully located " << m_summedDeposits->size()
            << " MCRichSummedDeposits at " << m_mcRichSummedDepositsLocation << endmsg;

  // Clear time sample cache
  tscache.clear();
  tscache.reserve( m_summedDeposits->size() );

  // Run analog sim
  const StatusCode sc = Analog();
  if ( sc.isFailure() ) { return sc; }

  // run digital sim and return
  return Digital();
}

StatusCode DetailedFrontEndResponse::Analog()
{
  _ri_debug << "Analogue Simulation" << endmsg;

  for ( auto * SumDep : *m_summedDeposits )
  {

    _ri_verbo << "Summed Deposit " << SumDep->key() << endmsg;

    auto * props = actual_base->DecodeUniqueID( SumDep->key() );
    if ( !props   )
    {
      //std::ostringstream mess;
      //mess << "ID " << (*iSumDep)->key() << " has no RichPixelProperties";
      //Warning( mess.str(), StatusCode::FAILURE, 0 );
      continue;
    }

    const auto * readOut = props->Readout();
    if ( !readOut )
    {
      std::ostringstream mess;
      mess << "ID " << SumDep->key() << " has no RichPixelReadout";
      Warning( mess.str(), StatusCode::FAILURE, 0 ).ignore();
      continue;
    }

    const auto * shape = readOut->Shape();
    if ( shape )
    {

      // Create time sample for this summed deposit
      tscache.emplace_back( TimeData( SumDep,
                                      RichTimeSample( readOut->FrameSize(),
                                                      readOut->BaseLine() ) ) );
      auto & ts = tscache.back().second;

      // Retrieve vector of SmartRefs to contributing deposits (non-const)
      const auto & deposits = SumDep->deposits();

      for ( const auto dep : deposits )
      {

        _ri_verbo << " Deposit " << dep->key()
                  << " from '" << objectLocation( dep->parentHit()->parent() ) << "'" << endmsg
                  << "  -> TOF     = " << dep->time() << endmsg
                  << "  -> Energy  = " << dep->energy() << endmsg;

        const auto rich = dep->parentHit()->rich();

        // Course cut on deposit TOF ( -50ns to 50ns )
        if ( fabs(dep->time()) < 50 )
        {

          // Bin zero
          const int binZero = (int)dep->time();

          // origin time
          double binTime = m_timeShift[rich] - binZero;

          // dead region
          const bool dead = ( binZero < 0 && binZero > -50 );

          // electrons
          const double e  =
            ( dead ? 0 : (dep->energy()*m_Calibration) + m_gaussNoise()/el_per_adc );

          // Loop over time sample and fill for this deposit
          for ( unsigned int bin = 0; bin < ts.size(); ++bin )
          {
            binTime += 25. / readOut->FrameSize();
            ts[bin] += ( dead ? -999999 : (*shape)[binTime] * e );
          }

        }

      } // MCRichDeposit loop

    } // if shape
    else
    {
      Warning( "Summed deposit has no associated RichShape" ).ignore();
    }

  }

  return StatusCode::SUCCESS;
}

StatusCode DetailedFrontEndResponse::Digital()
{
  _ri_debug << "Digital Simulation" << endmsg;

  // new RichDigit container to Gaudi data store
  LHCb::MCRichDigits * mcRichDigits = new LHCb::MCRichDigits();
  put( mcRichDigits, m_mcRichDigitsLocation );

  for ( auto & ts : tscache )
  {

    auto * props = actual_base->DecodeUniqueID( ts.first->key() );
    const auto * readOut = props->Readout();
    if ( readOut )
    {

      const double temp_threshold = m_gaussThreshold()/el_per_adc + readOut->BaseLine();
      if ( readOut->ADC()->process(ts.second,temp_threshold) )
      {

        auto * newDigit = new LHCb::MCRichDigit();
        mcRichDigits->insert( newDigit, ts.first->key().pixelID() );

        // Create MCRichHit links
        const auto & deps = ts.first->deposits();
        LHCb::MCRichDigitHit::Vector hitVect;
        hitVect.reserve( deps.size() );
        for ( const auto dep : deps )
        {
          hitVect.emplace_back( LHCb::MCRichDigitHit( *(dep->parentHit()), dep->history() ) );
        }
        newDigit->setHits( hitVect );

        // Store overall history info
        newDigit->setHistory( ts.first->history() );

      }

    } // readout exists

  } // loop over time samples

  _ri_debug << "Registered " << mcRichDigits->size()
            << " MCRichDigits at " << m_mcRichDigitsLocation << endmsg;

  return StatusCode::SUCCESS;
}
