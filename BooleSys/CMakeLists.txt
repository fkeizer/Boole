################################################################################
# Package: BooleSys
################################################################################
gaudi_subdir(BooleSys v32r2)

gaudi_depends_on_subdirs(Digi/Boole
)

gaudi_add_test(QMTest QMTEST)
