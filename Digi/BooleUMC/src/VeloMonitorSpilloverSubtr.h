// $Id: VeloMonitorSpilloverSubtr.h,v 1.1.1.1 2009-09-19 23:12:04 tskwarni Exp $
#ifndef VELOMONITORSPILLOVERSUBTR_H 
#define VELOMONITORSPILLOVERSUBTR_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
#include <string>

/** @class VeloMonitorSpilloverSubtr VeloMonitorSpilloverSubtr.h
 *  
 *
 *  @author 
 *  @date   2009-09-11
 */
class VeloMonitorSpilloverSubtr : public GaudiHistoAlg {
public: 
  /// Standard constructor
  VeloMonitorSpilloverSubtr( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~VeloMonitorSpilloverSubtr( ); ///< Destructor

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;    ///< Algorithm finalization

protected:

private:

  std::string m_inputLocation;
  std::vector<std::string> m_spillNames;
  std::vector<std::string> m_spillPaths;

};
#endif // VELOMONITORSPILLOVERSUBTR_H
