from Gaudi.Configuration import *

importOptions("$APPCONFIGOPTS/Boole/Default.py")
importOptions("$APPCONFIGOPTS/Boole/DataType-2012.py")

from Configurables import GaudiSequencer
seqGenFSR = GaudiSequencer("GenFSRSeq")
seqGenFSR.Members += [ "GenFSRMerge" ]
seqGenFSR.Members += [ "GenFSRLog" ]

ApplicationMgr().TopAlg += [seqGenFSR]

from Configurables import LHCbApp, Boole
LHCbApp().DDDBtag   = "dddb-20130929-1"
LHCbApp().CondDBtag = "sim-20130522-1-vc-md100"

Boole().MergeGenFSR = True

from PRConfig import TestFileDB
TestFileDB.test_file_db["genFSR_2012_sim"].run(configurable=Boole())
