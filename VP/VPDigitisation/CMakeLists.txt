################################################################################
# Package: VPDigitisation
################################################################################
gaudi_subdir(VPDigitisation v1r7)

gaudi_depends_on_subdirs(Det/VPDet
                         Event/DigiEvent
                         Event/LinkerEvent
                         Event/MCEvent
                         GaudiAlg
                         Kernel/LHCbKernel)

find_package(GSL)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(VPDigitisation
                 src/*.cpp
                 INCLUDE_DIRS GSL AIDA Event/DigiEvent
                 LINK_LIBRARIES GSL VPDetLib LinkerEvent MCEvent GaudiAlgLib LHCbKernel)

